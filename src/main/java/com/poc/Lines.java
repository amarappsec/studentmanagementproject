package com.poc;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Lines {
    private String line;
}
