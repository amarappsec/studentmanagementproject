package com.poc;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class FileIoTest {
    public static void main(String[] args) {
        try {
            /*Lines lineObj= new Lines();
            lineObj.setLine("amar");
            lineObj.getLine();*/
            //List<String> lines=Files.lines(Paths.get("JavaConcepts")).collect(Collectors.toList());
            //Files.lines(Paths.get("JavaConcepts")).forEach(System.out::println);

            /*for (String line: lines) {
                System.out.println(line);
            }*/

            Files.lines(Paths.get("JavaConcepts")).forEach(System.out::println);
            /*for (String line: lines) {
                System.out.println(line);
            }*/
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


